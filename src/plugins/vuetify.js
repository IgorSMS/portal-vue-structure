import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import colors from 'vuetify/lib/util/colors'
Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: {
          base: "#021526",
          darken1: "#ffffff",
        },
        secondary: colors.grey.darken1,
        accent: colors.shades.black,
        error: colors.red.accent3,
        "active-item": "#042959",
      },
    },
    options: { customProperties: true },
  }
  });
